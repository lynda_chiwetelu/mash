<?php

namespace Controllers;

use Helpers\Request;
use Models\Comment;
use Models\Log;

class CommentController extends BaseController {

		public function index($vars){

			$channel = "youtube";

			$channelId = self::getChannelId(CHANNEL);

			if($channelId){

				//remove Data first
				$delete = Comment::where('channel',$channel)->delete();

				$log_id = self::logStartTime();
				

				$next = 'start';

				$count = 0;
				$limit = 100;

				$estimated_turns = ceil(LIMIT/$limit);

				while($next){

					if($next == 'start'){
						$next = null; //if $next variable is set to start,make it null because it isn't to be used in actual call
					}

					$result = self::getComments($channelId,$next,$limit);

					$result = json_decode($result, true);	

					$items = $result['items'];

					$next = $result['nextPageToken'];
					$info = $result['pageInfo'];
					$all_data = [];
					foreach ($items as $item) {
						$comment_id = $item['id'];
						$comment = $item['snippet']['topLevelComment']['snippet']['textOriginal'];
						$author_name = $item['snippet']['topLevelComment']['snippet']['authorDisplayName'];
						$author_image_url = $item['snippet']['topLevelComment']['snippet']['authorProfileImageUrl'];
						$created_at = $item['snippet']['topLevelComment']['snippet']['publishedAt'];
						$updated_at = $item['snippet']['topLevelComment']['snippet']['updatedAt'];

						$data = ['comment'=>$comment,'channel_comment_id'=>$comment_id,
								'channel'=>$channel,'author_username'=>$author_name,'created_at'=>$created_at,
								'updated_at'=>$updated_at,'author_profile_image'=>$author_image_url];
						$all_data[] = $data;
				
					}

					$result = Comment::insert($all_data);
					$count++;

					sleep(20);

					if($count == $estimated_turns){

						//Calls should end but confirm this

						$number_of_comments = self::countComments();
						$diff = LIMIT - $number_of_comments;

						if($diff >= 1 ) {
							$limit = $diff;
						}else{
							self::logEndTime($log_id);
							break; //end loop because it means data is complete 
						}

					}

					if($count > $estimated_turns){
						self::logEndTime($log_id);
						break;
					}

					

				}

				
				
			} else{

				echo json_encode(['error'=>'Error Getting Channel ID']);

			}		
		}

		public function getChannelId($channel){

			$request = new Request();
			$url = YOUTUBE_API_URL;
			$key = YOUTUBE_API_KEY;
			$resource = 'channels';
			$params = ['forUsername'=>$channel,'key'=>YOUTUBE_API_KEY,'part'=>'id'];
			$query = http_build_query($params);
			$url = $url.'/'.$resource.'?'.$query;
			$channel = $request->get($url);
			$response = json_decode($channel, true);
			$channel_id = $response['items'][0]['id'];
			return $channel_id;

		}

		public static function getComments($channelId,$pageToken=null,$limit=100){

			$request = new Request();
			$url = YOUTUBE_API_URL;
			$key = YOUTUBE_API_KEY;
			$resource = "commentThreads";
			$params =['part'=>'snippet','maxResults'=>$limit,'allThreadsRelatedToChannelId'=>$channelId,'key'=>YOUTUBE_API_KEY];
			if($pageToken){
				$params['pageToken'] = $pageToken;
			}
			$query = http_build_query($params);
			$url = $url.'/'.$resource.'?'.$query;
			$comments = $request->get($url);
			return $comments;


		}

		private static function logStartTime(){

			//Log Start Time
				$now = new \DateTime;
				$log = Log::create(['type'=>'pulled youtube comments','start'=>$now]);
				return $log['id'];

		}

		private static function logEndTime($log_id){

			//Log End Time
			$now = new \DateTime;
			$log = Log::find($log_id);
			$log->end = $now;
			$log->save();
			
		}

		public static function countComments(){
			return Comment::count();
		}


		public function displayComments(){

			require 'views/index.html';
			die;

		}

		public function getCommentsFromDb(){

			$comments = Comment::all()->toArray();
			echo json_encode($comments,true);
			die;

		}

		public function getLastFetchedTime(){
			$log = Log::orderBy('end','desc')->first();
			$duration = self::getDuration($log['end']);
			echo json_encode(['duration'=>$duration]);

		}

		public  static function getDuration($datetime, $full=false){
			$now = new \DateTime;
                $ago = new \DateTime($datetime);
                $diff = $now->diff($ago);

                $diff->w = floor($diff->d / 7);
                $diff->d -= $diff->w * 7;

                $string = array(
                    'y' => 'year',
                    'm' => 'month',
                    'w' => 'week',
                    'd' => 'day',
                    'h' => 'hour',
                    'i' => 'minute',
                    's' => 'second',
                );
                foreach ($string as $k => &$v) {
                    if ($diff->$k) {
                        $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                    } else {
                        unset($string[$k]);
                    }
                }

                if (!$full) $string = array_slice($string, 0, 1);
                return $string ? implode(', ', $string) . ' ago' : 'just now';
		}




}
