var mashableApp = angular.module("mashableApp", ['angularMoment']);
mashableApp.controller("CommentListCtrl", function($scope, $http) {

	$scope.loadingComments = false;
	$scope.duration = "";
	$scope.comments = []
	

	$http.get('/get_comments').success(function(data){
		$scope.error = false;
		$scope.comments = data;
		$scope.count = data.length;
        $scope.loadingComments = true;
 
		
		
		

	}).error(function(){
		$scope.error = true
		$scope.loadingComments = false;
		

	});


	$http.get('/get_last_fetched_time').success(function(data){
		$scope.errorDuration = false;
		$scope.duration = data;
		

	}).error(function(){
		$scope.errorDuration = true

	});

});

