<?php


namespace Models;

use \Illuminate\Database\Eloquent\Model;


class Log extends Model {
	
	protected $table = 'logs';

	protected $fillable = ['start','type','end'];

	
}
