<?php


namespace Models;

use \Illuminate\Database\Eloquent\Model;


class Comment extends Model {
	
	protected $table = 'comments';

	protected $fillable = ['comment','channel_comment_id','channel','author_username','created_at','updated_at','author_profile_image'];

	
}
