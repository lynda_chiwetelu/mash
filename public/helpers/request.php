<?php

namespace Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use GuzzleHttp\Psr7;

class Request 
{

	private $base_uri;
	private $headers;


	public function __construct($headers=[]){

		$this->headers = $headers;

	}



	public function get($url){
	
		  $client = new Client(['debug' => true]);

		  $request = new GuzzleRequest('GET', $url,$this->headers);

		  $response = $client->send($request);

		  $body = $response->getBody();

		  $contents = (string) $body;
		  return $body;

	}



}
