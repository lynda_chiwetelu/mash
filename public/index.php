<?php

session_start();

require '../settings.php';
require 'vendor/autoload.php';

use Models\Database;
use Helpers\LoginChecker;
new Database();



//ROUTING

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
    require 'routes.php';
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];


// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}

$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);


switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];

        //check for authentication 
        $open_routes = ['/fetch_comments','/login','/logout']; //leave api fetch routes open for cron
        if(!in_array($uri, $open_routes)){
            $logged_in = LoginChecker::verify();
            if(!$logged_in){
                header('Location:'.BASE_URL.'login');
            }
        }       
        

        // ... call $handler with $vars
        $arr = explode(':', $handler);
        (new $arr[0])->$arr[1]($vars);
        break;
}

//require 'views/index.html';
