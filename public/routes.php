<?php


//GENERAL GUIDE ROUTES SHOULD BE ADDED HERE
$r->addRoute('GET', '/fetch_comments', 'Controllers\CommentController:index');

$r->addRoute('GET', '/', 'Controllers\CommentController:displayComments');

$r->addRoute('GET', '/get_comments', 'Controllers\CommentController:getCommentsFromDb');

$r->addRoute('GET', '/get_last_fetched_time', 'Controllers\CommentController:getLastFetchedTime');

$r->addRoute('GET', '/login', 'Controllers\AuthController:loginForm');

$r->addRoute('POST', '/login', 'Controllers\AuthController:processLogin');

$r->addRoute('GET', '/logout', 'Controllers\AuthController:logout');


