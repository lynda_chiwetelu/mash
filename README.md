#WELCOME TO MASHABLE COMMENTS

Steps to running this App
-----------------------------
WebServer with PHP installed and running.

1. Make sure MySQL and Composer are installed

2. Pull the project folder.

3. Open the "migrations" folder and run the scripts in order

4. run `composer install` in /public

5. Now run `composer dumpautoload -o`

6. If you're using Apache, add a .htaccess file and place the following code in it

```
RewriteEngine On

RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-l

RewriteRule ^(.+)$ index.php?url=$1 [QSA,L]

```

If Nginx, or others, rewrite all requests for paths and files not found to index.php

7. Create a file named settings.php on the same level as the `public` folder 
define the following in it 

```
defined('YOUTUBE_API_KEY') or define('YOUTUBE_API_KEY', 'yourapikey');
defined('YOUTUBE_API_URL') or define('YOUTUBE_API_URL','url');
defined('CHANNEL') or define('CHANNEL','mashable');
defined('DBDRIVER') or define('DBDRIVER','mysql');
defined('DBNAME') or define('DBNAME','');
defined('DBHOST') or define('DBHOST','localhost');
defined('DBUSER') or define('DBUSER','root');
defined('DBPASS') or define('DBPASS','');
defined('LIMIT') or define('LIMIT','1000');
defined('USERNAME') or define('USERNAME',''); //admin username for login
defined('PASSWORD') or define('PASSWORD',''); //admin password for login 
defined('BASE_URL') or define('BASE_URL','yourhosturl');

```
and replace with your own values as you please

8. Set up host. 

9. Run App
